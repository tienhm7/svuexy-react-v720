// ** Core JWT Import
import useJwt from '@src/@core/auth/jwt/useJwt'
import axiosIns from '@src/libs/axiosIns'
import jwtOverrideConfig from './jwtOverrideConfig'

const { jwt } = useJwt(axiosIns, jwtOverrideConfig)

export default jwt
