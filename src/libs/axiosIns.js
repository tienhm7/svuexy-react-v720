// axios
import axios from 'axios'

const API_URL = 'http://l8api.test/api/'

const axiosIns = axios.create({
  // You can add your headers here
  // ================================
  baseURL: API_URL,

  headers: {
    'content-type': 'application/json'
  },

  paramsSerializer: (params) => queryString.stringify(params)
})

export default axiosIns
